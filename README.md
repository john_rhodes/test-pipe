
# Get Started
1. Dependencies
* Python is installed
* Python module Pipenv is installed

2. clone repo into desired location

        git clone git@bitbucket.org:<workspace>/<repo>.git

3. run the following to setup virtual environment and install from Pipfile.lock

        pipenv sync

4. activate venv

        source .venv/bin/activate

5. navigate to meltano directory
6. run the following to setup

        meltano install

7. Check the environment file is available (.env). If not add it manually.

8. define job_id

        JOB_ID='salesforce_to_snowflake_test'

9. execute job

        meltano --environment=dev elt tap-salesforce target-snowflake --job_id=$JOB_ID